extract <- open_dataset("data/Spatial_extract_V1_2.parquet") %>%
  filter(ISO_A0 == "THA",
         T_res == "Month",
         calendar_start_date == "2022-12-01") %>%
  collect()
