# Interactive Time-series

In this chapter, we'll explore how to create dynamic and interactive visualizations using the `{highcharter}` package in R. This package serves as an R wrapper for the Highcharts JavaScript charting library, enabling a wide variety of customizable and interactive charts. We will guide you through examples of single-series charts, multi-series charts, and charts with multiple axes.

::: callout-warning
## License Consideration

The `{highcharter}` package depends on the [Highcharts](https://www.highcharts.com/) library, which is a commercial product. Highcharts is available under a free non-commercial license or a commercial license. Before using `{highcharter}` in your project, please review the licensing terms to ensure compliance with your intended use case.
:::

## Getting Started

### Loading Required Libraries and Data

First, let's load the necessary libraries and the pre-filtered data. We'll use `{highcharter}` for charting, `{data.table}` for data manipulation, and `{magrittr}` for piping operations.

```{r}
#| message: false
#| warning: false
library(highcharter)
library(data.table)
library(magrittr)
```

Next, load the pre-filtered datasets for dengue cases and precipitation.

```{r}
dengue_thailand <- fread("data/dengue_thailand.csv")

# Load the pre-filtered precipitation dataset
precipitation <- fread("data/thailand_monthly_precipitation.csv")
precipitation$date <- as.Date(precipitation$date)
```

### Creating a Basic Line Chart

We'll start by creating a simple line chart that visualizes the number of dengue cases over time. This is achieved using the `highchart()` function with the `hc_add_series()` function to add data to the chart.

```{r}
highchart(type = "stock") %>%
    hc_add_series(
        dengue_thailand,
        hcaes(x = calendar_start_date, y = dengue_total),
        type = "line",
        lineWidth = 1,
        color="#FF6633",
        name= "Dengue cases"
    )
```

#### Breakdown of the Code

-   **`highchart(type = "stock")`**: Initializes a Highcharts chart with a "stock" type, suitable for time-series data.

-   **`hc_add_series()`**: Adds the data series to the chart.

    -   **`dengue_thailand`**: The dataset containing the dengue case data.

    -   **`hcaes(x = calendar_start_date, y = dengue_total)`**: Maps the `calendar_start_date` column to the x-axis (time) and the `dengue_total` column to the y-axis (dengue cases).

    -   **`type = "line"`**: Specifies that the data should be visualized as a line chart.

    -   **`lineWidth = 1`**: Sets the line thickness to 1 unit.

    -   **`color = "#FF6633"`**: Sets the line color to a specific orange hue.

    -   **`name = "Dengue cases"`**: Labels the series "Dengue cases" in the chart legend.

### Creating a Chart with Multiple Axes

In some cases, you might want to compare multiple datasets on the same chart, particularly when these datasets have different units or scales (e.g., dengue cases vs. precipitation). This can be accomplished by creating a chart with multiple y-axes.

### `hc_yAxis_multiples()`: Configuring Multiple Y-Axes

The `hc_yAxis_multiples()` function allows you to define multiple y-axes on the same chart. This is useful when you need to display different types of data, such as a count of cases and a measurement of precipitation, on separate axes.

#### Example of a Double-Axis Chart

In the following example, we plot dengue cases on one y-axis and precipitation on another. The two axes are stacked vertically, each occupying 50% of the chart.

```{r}
highchart(type = "stock") %>%
    hc_yAxis_multiples(
        list(title = list(text = "<b>Cases</b>"),
             opposite = FALSE,
             height = "50%",
             min = 0),
        list(title = list(text = "<b>Precipitation (mm)</b>"),
             showLastLabel = TRUE,
             offset = 0,
             top = "50%",
             height = "50%",
             opposite = FALSE,
             reversed = TRUE,
             min = 0)
    ) %>%
    hc_add_series(
        dengue_thailand,
        hcaes(x = calendar_start_date, y = dengue_total),
        type = "line",
        lineWidth = 1,
        color = "#FF6633",
        name = "Dengue cases",
        yAxis = 0
    ) %>%
    hc_add_series(
        precipitation,
        hcaes(x = date, y = precipitation),
        type = "column",
        color = "#03aaf9",
        name = "Precipitation",
        yAxis = 1
    )
```

#### Breakdown of the Code

-   **`hc_yAxis_multiples()`**: Defines multiple y-axes for the chart.

    -   The first axis is configured for dengue cases:

        -   **`title = list(text = "<b>Cases</b>")`**: The title "Cases" is displayed on the axis, with bold formatting.

        -   **`opposite = FALSE`**: The axis is positioned on the left side of the chart.

        -   **`height = "50%"`**: The axis occupies the top 50% of the chart height.

        -   **`min = 0`**: The axis starts from 0.

    -   The second axis is configured for precipitation:

        -   **`title = list(text = "<b>Precipitation (mm)</b>")`**: The title "Precipitation (mm)" is displayed on the axis.

        -   **`top = "50%"`**: The axis starts from the 50% mark of the chart height, below the first axis.

        -   **`height = "50%"`**: The axis occupies the bottom 50% of the chart.

        -   **`reversed = TRUE`**: The axis is reversed, with higher values at the bottom, often used for precipitation or other cumulative measures.

-   **`hc_add_series()`**: Adds data series to the respective y-axes.

    -   **First series (dengue cases)**:

        -   **`dengue_thailand`**: The dataset containing the dengue cases.

        -   **`hcaes(x = calendar_start_date, y = dengue_total)`**: Maps the `calendar_start_date` column to the x-axis and the `dengue_total` column to the first y-axis.

        -   **`yAxis = 0`**: Links this series to the first y-axis (index 0).

    -   **Second series (precipitation)**:

        -   **`precipitation`**: The dataset containing the precipitation data.

        -   **`hcaes(x = date, y = precipitation)`**: Maps the `date` column to the x-axis and the `precipitation` column to the second y-axis.

        -   **`yAxis = 1`**: Links this series to the second y-axis (index 1).

The `{highcharter}` package provides a powerful and flexible framework for creating interactive charts in R. By using functions like `hc_add_series()` and `hc_yAxis_multiples()`, you can build complex visualizations that effectively communicate multiple data series in a single chart. Whether you're creating basic line charts or more advanced multi-axis charts, `{highcharter}` offers a wide range of customization options to suit your needs.
