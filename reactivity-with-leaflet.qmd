# Reactivity with Leaflet

## Static vs. Dynamic Maps: Leveraging `leafletProxy` for Real-Time Updates

When creating interactive maps in R with Leaflet, understanding the difference between static and dynamic map updates is key. Static maps are rendered once when the app starts using the `leaflet()` function. These maps contain elements that do not change, like base tiles and initial markers. However, in many applications, you want to update the map dynamically based on user interactions—without reloading the entire map. This is where `leafletProxy()` becomes essential.

The `leafletProxy()` function allows you to update specific elements on the map without resetting the entire view. For example, you can modify a polygon's color, add new markers, or update map layers based on user input. By using `leafletProxy()`, the map retains its current zoom level and position, while only the necessary elements are updated dynamically.

## Enhancing Interactivity with the `leafletProxy()` Function

**Example: Filtering Dengue Data by Total Cases**

Suppose you're building an app that displays dengue data. You want users to filter this data by total cases and see the updates on the map in real-time. Here's how you can do it using `leafletProxy()`:

```{shinylive-r}
#| standalone: true
#| components: [editor, viewer]
#| layout: vertical
#| viewerHeight: 400
library(shiny)
library(leaflet)
library(RColorBrewer)  # For color schemes

# Load datasets
province_centroid <- read.csv("https://espace-dev.frama.io/geohealth/rshiny4onehealth/data/thailand_province_centroid.csv")
dengue_extract <- read.csv("https://espace-dev.frama.io/geohealth/rshiny4onehealth/data/dengue_2022-12.csv")

# Merge datasets
dengue <- merge(dengue_extract, province_centroid, by.x = "adm_1_name", by.y = "province")

# Define the UI
ui <- bootstrapPage(
  # Custom style to ensure the map takes up the full viewport
  tags$style(type = "text/css", "html, body {width:100%;height:100%}"),
  
  # Leaflet map output
  leafletOutput("map", width = "100%", height = "100%"),
  
  # Control panel with a slider and a legend toggle
  absolutePanel(top = 10, right = 10,
                sliderInput("range", "Cases", min(dengue$incidence_rate), max(dengue$incidence_rate),
                            value = c(min(dengue$incidence_rate), max(dengue$incidence_rate)), step = 1  # Slider for selecting magnitude range
                ),
                checkboxInput("legend", "Show legend", TRUE)  # Checkbox to toggle the legend
  )
)

# Define the server logic
server <- function(input, output, session) {
  
  # Reactive expression to filter data based on the selected magnitude range
  filteredData <- reactive({
    dengue[dengue$incidence_rate >= input$range[1] & dengue$incidence_rate <= input$range[2],]
  })
  
  # Use BrBG palette for color mapping
  colorpal <- reactive({
    colorNumeric(palette = "BrBG", domain = dengue$incidence_rate)
  })
  
  # Render the initial Leaflet map
  output$map <- renderLeaflet({
    leaflet(dengue) %>%
      addTiles() %>%  # Add base tiles
      fitBounds(~min(long), ~min(lat), ~max(long), ~max(lat))  # Fit map bounds to data
  })
  
  # Observer to update the map when the filtered data changes
  observe({
    pal <- colorpal()  # Get the color palette
    
    leafletProxy("map", data = filteredData()) %>%  # Use leafletProxy to update the map
      clearMarkers() %>%  # Clear existing markers
      addCircleMarkers(        # Add new circles based on filtered data
        lng = ~long, lat = ~lat, 
        radius = ~sqrt(incidence_rate) * 3,  # Set circle radius based on square root of incidence rate
        weight = 1,           # Circle border weight
        color = "#777777",     # Circle border color
        fillColor = ~pal(incidence_rate), # Fill color based on number of cases
        fillOpacity = 0.7,     # Circle opacity
        popup = ~paste(adm_1_name, ": Incidence Rate", incidence_rate)  # Popup text showing province and rate
      )
  })
  
  # Observer to update the legend based on the user's selection
  observe({
    proxy <- leafletProxy("map", data = dengue)  # Access the map proxy
    
    proxy %>% clearControls()  # Clear existing legend
    if (input$legend) {  # If the legend checkbox is selected
      pal <- colorpal()  # Get the color palette
      proxy %>% addLegend(  # Add the legend
        position = "bottomright",  # Legend position
        pal = pal,  # Palette function
        values = ~incidence_rate,  # Data for the legend
        title = "Incidence Rate"  # Title for the legend
      )
    }
  })
}

# Run the Shiny app
shinyApp(ui, server)
```

**What’s Happening?**

1.  **Static Map Creation**: The `leaflet()` call inside `renderLeaflet()` initializes the map with tiles and sets the initial view bounds. This part of the map remains static—unchanged as the app runs.

2.  **Dynamic Updates with `leafletProxy()`**: The `leafletProxy()` function is used to update map elements (like circle markers and legends) in real-time as the user interacts with the UI. For example, when the user adjusts the total cases slider, `leafletProxy()` updates the displayed markers on the map without reloading the entire map.

3.  **Observer Patterns**: Separate observers (`observe()`) handle different aspects of dynamic updates. One observer updates the circles based on the filtered data, while another updates the legend according to the user's preferences.

## Cross-Interaction Between Map and UI Elements

A key strength of R Leaflet is its ability to create cross-interactions between the map and other UI elements. This means the map can respond to user inputs, and UI elements can react based on map interactions.

**Example Scenario: Synchronizing Map and Plots**

Imagine a scenario where your app displays a map of dengue cases and a histogram showing the distribution of dengue cases in the visible region. You want the histogram to update based on the visible region in the map—panning or zooming the map changes the histogram dynamically.

```{shinylive-r}
#| standalone: true
#| components: [editor, viewer]
#| layout: vertical
#| viewerHeight: 800
library(shiny)
library(leaflet)

# Load datasets
province_centroid <- read.csv("https://espace-dev.frama.io/geohealth/rshiny4onehealth/data/thailand_province_centroid.csv")
dengue_extract <- read.csv("https://espace-dev.frama.io/geohealth/rshiny4onehealth/data/dengue_2022-12.csv")

# Merge datasets
dengue <- merge(dengue_extract, province_centroid, by.x = "adm_1_name", by.y = "province")


# Define the UI
ui <- fluidPage(
  fluidRow(
    column(
      width = 12,
      leafletOutput("map", height = "50vh")  # Create a leaflet map output taking half the vertical space
    )
  ),
  fluidRow(
    column(
      width = 12,
      plotOutput("region_plot", height = "50vh")  # Create a plot output taking half the vertical space
    )
  )
)

# Define the server logic
server <- function(input, output, session) {

  # Render the initial leaflet map with base tiles
  output$map <- renderLeaflet({
    leaflet(dengue) %>%
      addTiles() %>%  # Add default OpenStreetMap tiles
      fitBounds(~min(long), ~min(lat), ~max(long), ~max(lat))  # Set map bounds to fit all data points
  })

  # Observe changes in the map bounds and update the visible data and histogram
  observeEvent(input$map_bounds, {
    bounds <- input$map_bounds  # Get the current map bounds (visible region)

    # Filter data to include only points within the visible map region
    visible_data <- dengue[
      dengue$long >= bounds$west & dengue$long <= bounds$east &
      dengue$lat >= bounds$south & dengue$lat <= bounds$north,]

    # Render the histogram based on the filtered visible data
    output$region_plot <- renderPlot({
      if (nrow(visible_data) > 0) {
        hist(visible_data$incidence_rate, 
             main = "Incidence Rate Distribution in Visible Region", 
             xlab = "Incidence Rate")
      } else {
        plot.new()  # Clear the plot if no data is available
        text(0.5, 0.5, "No data available in this region", cex = 1.5)  # Display a message
      }
    })

    # Update the map markers based on the visible data
    leafletProxy("map", data = visible_data) %>%
      clearMarkers() %>%  # Clear existing markers
      addCircleMarkers(
        lng = ~long, lat = ~lat,
        radius = ~sqrt(incidence_rate) * 3,,  # Circle radius proportional to incidence rate
        color = "grey",  # Marker color
        fillOpacity = 0.5,
        popup = ~paste(adm_1_name, ": Incidence Rate", incidence_rate)  # Popup text showing province and rate
      )
  })
}

# Run the Shiny app
shinyApp(ui, server)
```

**What’s Happening?**

1.  **Initial Map Setup**: The `leaflet()` function initializes the map with tiles and sets the initial view bounds using `fitBounds()` to encompass the entire dengue dataset. This creates the static foundation of the map.

2.  **Dynamic Map and Plot Updates**: The `observeEvent(input$map_bounds, {...})` function listens for changes in the map's visible bounds. Every time the user pans or zooms the map, this observer triggers.

3.  **Filtering Data Based on Map Bounds**: The `visible_data` variable contains the subset of dengue data that falls within the currently visible map region. This filtered data is used to update both the histogram and the map markers.

4.  **Histogram Update**: The `renderPlot()` function generates a histogram based on the total dengue cases visible within the map's bounds. This provides a dynamic view of dengue case distribution in the visible area.

5.  **Marker Update on Map**: The `leafletProxy()` function updates the map without reloading. It clears existing markers and adds new ones based on the filtered data. The size of each circle marker is proportional to the square root of `dengue_total`, and the markers include a popup showing the province and the total number of dengue cases.

**Result:**

-   Users can see markers on the map that correspond to dengue cases visible in the current map view. As they pan or zoom the map, the markers update in real-time, and the histogram adjusts to reflect the distribution of dengue cases in the visible area. This setup provides a powerful, interactive visualization that links spatial data with statistical insights seamlessly.

This example demonstrates how to create an interactive and responsive application using Shiny and Leaflet, where the map and other UI elements (like plots) are tightly synchronized.

## Capturing and Responding to User Events

R Leaflet provides robust mechanisms for capturing user interactions, known as events. These events allow your app to respond to specific actions like map clicks, zoom changes, or dragging.

**Types of User Events:**

-   **Object Events**: Triggered when a user interacts with specific map objects (e.g., clicking on a marker).

-   **Map Events**: Triggered by interactions with the map itself (e.g., clicking on the map background, panning, zooming).

**Example: Responding to Map Clicks**

Here's a simple example of capturing a click event on the map and displaying the coordinates of the click in the UI.

```{shinylive-r}
#| standalone: true
#| components: [editor, viewer]
#| layout: vertical
#| viewerHeight: 600
library(shiny)
library(leaflet)
# Define the UI with a map and a text output
ui <- fluidPage(
  leafletOutput("map"),  # Map output
  textOutput("click_info")  # Text output for displaying click info
)

# Define the server logic
server <- function(input, output, session) {

  # Render the initial Leaflet map
  output$map <- renderLeaflet({
    leaflet() %>%
      addTiles()  # Add base tiles
  })

  # Observer to capture and respond to map clicks
  observeEvent(input$map_click, {
    click <- input$map_click  # Get the click event data

    # Display the clicked coordinates in the UI
    output$click_info <- renderText({
      paste("You clicked at:", click$lat, click$lng)
    })
  })
}

# Run the Shiny app
shinyApp(ui, server)

```

**What's Happening?**

-   **Capturing Click Events**: The `observeEvent(input$map_click, {...})` block listens for click events on the map. When a click occurs, the event data (latitudeand longitude of the click location) is captured and used within the observer.

-   **Dynamic Text Output**: The `output$click_info` element is updated with the coordinates of the click, providing immediate feedback to the user.

This chapter covers the basics of working with reactive elements in Leaflet within a Shiny app. By understanding how to effectively use `leafletProxy()`, synchronize map views with other UI components, and respond to user events, you can create highly interactive and responsive map-based applications.
