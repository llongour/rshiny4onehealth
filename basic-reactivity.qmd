# Basic reactivity

Reactivity is what makes Shiny applications come alive! It allows your apps to respond dynamically to user inputs, ensuring a smooth and engaging experience. This chapter breaks down the key reactive components in Shiny, explaining how they work and providing examples related to spatial epidemiology and the geography of health.

## What is Reactivity?

Reactivity in Shiny ensures that when users interact with your app—like moving a slider or typing in a text box—the app automatically updates the output. This dynamic interaction keeps users engaged and helps them visualize changes in real time.

## The Input Object: Capturing User Choices

### What It Is {.unnumbered}

The `input` object acts like the app's ears—it listens to all user interactions. Each input control, such as a slider or text box, has a unique ID, which allows the server to know what the user has chosen.

### Example {.unnumbered}

Imagine a public health app where users can select geographic regions to analyze the prevalence of diseases. This enables users to explore data and identify hotspots in spatial epidemiology.

```{shinylive-r}
#| standalone: true
#| components: [editor, viewer]
#| layout: vertical
#| viewerHeight: 400
library(shiny)

ui <- fluidPage(
  selectInput("region", "Select a region:", choices = c("Region A", "Region B", "Region C")),
  textOutput("selected_region")
)

server <- function(input, output) {
  output$selected_region <- renderText({
    paste("You have selected:", input$region)
  })
}

shinyApp(ui = ui, server = server)
```

### What’s Happening? {.unnumbered}

When the user selects a region from the dropdown menu, Shiny updates the `input$region` variable with the currently selected value. The `renderText()` function is called whenever `input$region` changes, resulting in the updated greeting being displayed on the UI. This process is facilitated by Shiny's reactive programming model, which efficiently tracks dependencies and updates only the necessary outputs, minimizing unnecessary computations.

## The Output Object: Displaying Results

### What It Is {.unnumbered}

The `output` object is the voice of your app. It shows results like plots, tables, or messages based on user input. Each output element has a unique ID.

### Example {.unnumbered}

When a user selects a region, the app could display a map showing disease prevalence in that area.

```{shinylive-r}
#| standalone: true
#| components: [editor, viewer]
#| layout: vertical
#| viewerHeight: 400
library(shiny)

ui <- fluidPage(
  selectInput("region", "Select a region:", choices = c("North", "South", "East", "West")),
  textOutput("prevalence")
)

server <- function(input, output) {
  # Example data
  disease_prevalence <- list(
    North = "5 cases per 1,000 people",
    South = "10 cases per 1,000 people",
    East = "3 cases per 1,000 people",
    West = "8 cases per 1,000 people"
  )
  
  output$prevalence <- renderText({
    paste("Disease Prevalence in", input$region, ":", disease_prevalence[[input$region]])
  })
}

shinyApp(ui = ui, server = server)

```

### What's Happening? {.unnumbered}

The `renderText()` function generates dynamic text that changes based on the selected region. It uses the `input$region` to access the relevant disease prevalence data from a predefined list. Whenever the user changes the selection, the output updates automatically, demonstrating Shiny's reactivity.

## Creating Dynamic Calculations with `reactive()`

### What It Is {.unnumbered}

The `reactive()` function lets you create dynamic calculations based on user input. It automatically updates whenever the input changes.

### Example {.unnumbered}

In a disease mapping app, users might enter the number of cases in specific regions. The app can then calculate and instantly display the incidence rate.

```{shinylive-r}
#| standalone: true
#| components: [editor, viewer]
#| layout: vertical
#| viewerHeight: 400
library(shiny)

ui <- fluidPage(
  numericInput("cases", "Enter number of cases:", value = 2500),
  numericInput("population", "Enter population size:", value = 1500000),
  textOutput("incidence_rate")
)

server <- function(input, output) {
  # Create a reactive expression to calculate the incidence rate
  incidence_rate <- reactive({
    req(input$population)  # Ensure population is greater than zero
    if (input$cases == 0) return(0)
    (input$cases / input$population) * 100000  # Calculate incidence rate per 100,000 people
  })

  output$incidence_rate <- renderText({
    paste("Incidence Rate:", round(incidence_rate(), 2), "per 100,000 people")
  })
}

shinyApp(ui = ui, server = server)
```

### What's Happening? {.unnumbered}

The `reactive()` function creates a reactive expression, `incidence_rate`, that calculates the incidence rate based on the number of cases and population size. This expression automatically re-evaluates whenever `input$cases` or `input$population` changes, ensuring that the latest calculation is displayed in `output$incidence_rate`. The `req()` function ensures the population size is valid before performing the calculation, preventing errors like division by zero.

### When to Use It

-   **Reusable Calculations**: Use `reactive()` when you need to perform a calculation that will be used in multiple places within your app.

-   **Dynamic Data Handling**: Ideal for any situation where a calculation depends on changing user inputs.

## Responding to Changes with `observe()`

### What It is

The `observe()` function allows your app to react to changes in inputs or reactive expressions. It's often used for side effects, like updating other UI elements or triggering alerts.

### Example {.unnumbered}

If the user enters vaccination rates, the app can automatically update to show the estimated impact on disease transmission.

```{shinylive-r}
#| standalone: true
#| components: [editor, viewer]
#| layout: vertical
#| viewerHeight: 400
library(shiny)

ui <- fluidPage(
  numericInput("vaccination_rate", "Vaccination rate (%):", value = 0),
  textOutput("impact_on_transmission")
)

server <- function(input, output) {
  observe({
    output$impact_on_transmission <- renderText({
      if (input$vaccination_rate > 80) {
        "Vaccination rate is sufficient to reduce transmission risk."
      } else {
        "Vaccination rate is below the threshold for herd immunity!"
      }
    })
  })
}

shinyApp(ui = ui, server = server)
```

### What's Happening ? {.unnumbered}

The `observe()` function monitors `input$vaccination_rate`. Whenever this input changes, the code inside the `observe()` block executes, updating the message displayed in `output$impact_on_transmission`. This dynamic response allows the app to react in real-time to user inputs, showing the potential impact of vaccination rates on disease transmission.

### When to Use It

-   **Side Effects and Notifications**: Use `observe()` when you need to perform actions that don’t return a value but need to occur in response to changes in input, like updating other UI elements or sending alerts.

## Event-Driven Actions with `observeEvent()`

### What It is {.unnumbered}

The `observeEvent()` function in Shiny lets you create observers that react only when specific events occur, such as button clicks. This is useful for actions that should only happen under certain conditions or at specific moments, rather than continuously.

### Example {.unnumbered}

When the user clicks "Submit," the app can log the current case counts and display an updated message. The message updates only when the button is clicked, regardless of any subsequent changes to the input.

```{shinylive-r}
#| standalone: true
#| components: [editor, viewer]
#| layout: vertical
#| viewerHeight: 400
library(shiny)

ui <- fluidPage(
  numericInput("case_count", "Enter current case count:", value = 0),
  actionButton("submit", "Submit"),
  textOutput("status")
)

server <- function(input, output) {
  # Observe the Submit button click event
  observeEvent(input$submit, {
    # Store the case count at the time of button click
    logged_count <- input$case_count
    
    # Update the status message
    output$status <- renderText({
      paste("Current case count of", logged_count, "logged.")
    })
  })
}

shinyApp(ui = ui, server = server)
```

### What's Happening ? {.unnumbered}

-   **`observeEvent(input$submit, {...})`**: The `observeEvent()` function listens for the "Submit" button click event. When the button is clicked, the observer's code block is executed.

-   **`logged_count <- input$case_count`**: The value of `input$case_count` is captured at the moment the button is clicked and stored in a local variable `logged_count`.

-   **`output$status <- renderText({...})`**: The status message is updated using the captured value of `logged_count`. This ensures that the displayed message reflects the case count at the time of the button click and does not change when `input$case_count` is updated afterward.

### When to Use It

-   **Event-Triggered Actions**: Use `observeEvent()` when you need to perform an action in response to specific events, such as button clicks or other user interactions.

-   **Non-Reactive Updates**: Ideal for scenarios where updates should occur based on discrete events rather than continuous changes.

::: callout-note
## Alternative : Using \`eventReactive()\`

**What It is**

The `eventReactive()` function allows you to create reactive expressions that update only in response to specific events, like button clicks. It’s useful when you need to perform computations or updates that depend on these events.

**When to Use It**

-   **Reusable Reactive Outputs**: Use `eventReactive()` when you need a reactive expression that depends on an event and can be reused in multiple outputs.

-   **Delayed Calculations**: Ideal for cases where you want to delay calculations or updates until a specific event occurs.

-   **Simplified Code**: Use `eventReactive()` to encapsulate event-driven logic, making server code cleaner and more organized.
:::

## Keeping Track of State with `reactiveValues()`

### What It is {.unnumbered}

The `reactiveValues()` function allows you to create a set of reactive values that can be modified. This is useful for maintaining state across multiple inputs or calculations.

### Example {.unnumbered}

You might want to keep track of cumulative case counts and vaccination numbers, enabling users to analyze trends over time.

```{shinylive-r}
#| standalone: true
#| components: [editor, viewer]
#| layout: vertical
#| viewerHeight: 400
library(shiny)

ui <- fluidPage(
  numericInput("new_cases", "Enter new cases:", value = 0),
  numericInput("new_vaccinations", "Enter new vaccinations:", value = 0),
  actionButton("add_data", "Add Data"),
  textOutput("summary")
)

server <- function(input, output) {
  values <- reactiveValues(total_cases = 0, total_vaccinations = 0)

  observeEvent(input$add_data, {
    values$total_cases <- values$total_cases + input$new_cases
    values$total_vaccinations <- values$total_vaccinations + input$new_vaccinations
  })

  output$summary <- renderText({
    paste("Total Cases:", values$total_cases, "| Total Vaccinations:", values$total_vaccinations)
  })
}

shinyApp(ui = ui, server = server)
```

### What's Happening ? {.unnumbered}

The `reactiveValues()` function initializes a list of reactive variables, `values`, to track total cases and vaccinations. When the "Add Data" button is clicked, the `observeEvent()` updates the totals based on user input. Thesummary is dynamically displayed using `renderText()`, providing users with updated information on the total cases and vaccinations without requiring a page refresh.

# Advanced tips for reactivity

## Real-Time Updates with `Sys.time()`

In some applications, you may want to show real-time information, such as the current system time, which updates every second.

**Example: Real-Time Clock**

```{shinylive-r}
#| standalone: true
#| components: [editor, viewer]
#| layout: vertical
#| viewerHeight: 400
library(shiny)

ui <- fluidPage(
  textOutput("currentTime")
)

server <- function(input, output, session) {
  output$currentTime <- renderText({
    invalidateLater(1000, session)  # Re-run this reactive every 1000 milliseconds (1 second)
    paste("Current time is:", Sys.time())
  })
}

shinyApp(ui = ui, server = server)
```

**What's Happening?**

`invalidateLater(1000, session)`: This function tells Shiny to re-run the reactive expression every 1000 milliseconds (1 second). It’s what enables the real-time update.

`Sys.time()`: Fetches the current system time, which is then displayed in the UI and updated every second.
